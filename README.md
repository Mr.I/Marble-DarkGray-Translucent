## Marble-DarkGray-Translucent

This is a modified version of [Marble-shell](https://github.com/imarkoff/Marble-shell-theme)'s dark gray theme. It (mainly) offers a bit more transparency comparing to the original Marble-gray-dark theme.

For simplicity, I didn't fork the whole project. This repo only contains the final product.


## Installation

To install, copy the repo to one of your themes directories:

* ~/.themes
* ~/.local/share/themes
* /usr/share/themes
* /usr/local/share/themes

Open your GNOME Tweaks tool, select the theme and enjoy.


## Minor Changes

* increased visibility for activities ripple
* increased width for workspace indicator
